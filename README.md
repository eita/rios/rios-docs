# Rios Software - Documentation

Welcome to the Rios Software documentation. Here you will find all information needed in order to deploy a complete Rios Software environment.

## What is Rios?

Rios is a methodology developed by Cooperativa EITA <http://eita.coop.br> to help social movements organizing their everyday colective work. It includes knowledge organization training and IT support to address online communication issues, as well as colaborative work - files cloud, calendar, tasks etc.

## What is Rios Software?

Rios Software is an ecosystem of open source softwares that gives the necessary IT support to the implementation of Rios. Rios Software is composed by community driven open source software, as well as by components developed by EITA. The general architecture of Rios software includes:

* OpenLDAP (authentication)
* Matrix/Riot (synapse protocol based chat)
* Nextcloud (file store)
* Matrixbrige (Nextcloud app to connect Nextcloud and Matrix)

## Let's start?

First of all, we need to say that these instruction were written for advanced users. Building the Rios Software is a complex task, and we were not able to write step-by-step instructions, because this would by simply too much complicated. We are constantly evolving these instructions, and contributions are very welcome. Moreover, these steps were enough to deploy Rios software on a Debian Stretch server. We are not aware of any big reason why it would not work on other GNU/Linux flavours, but please keep this in mind.

## NextCloud

Please install a modified version of Nextcloud available at: https://gitlab.com/eita/rios/rios-cloud-server. Use the branch `rios-vivos`.

## Matrix/Riot

Please install Matrix using git: https://github.com/matrix-org/synapse#synapse-development. Then, install Riot using git: https://github.com/vector-im/riot-web

## LDAP Support

LDAP is the user backend of the Rios interface. Until this moment, is it possible to:
*  On NextCloud: login and create and delete users and groups. NC reads LDAP parameters such as displayName and email.
*  On Synapse: only login.

We are working to on both systems to achieve a deeper integration, such as parameters syncing.

### OpenLDAP

    sudo apt-get update
    sudo apt-get install slapd ldap-utils

Configuring OpenLDAP Tutorial: https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-a-basic-ldap-server-on-an-ubuntu-12-04-vps

Attention: http://stackoverflow.com/questions/20673186/getting-error-for-setting-password-field-when-creating-generic-user-account-phpl#21195761

To restart open ldap, use:

    sudo service slapd {start|stop|status}

Afterwards, the apache ldap module must be installed:

    sudo apt-get install php7.0-ldap
    sudo phpenmod ldap
    sudo service apache2 restart

### PhpLDAPAdmin

**Don't use the package (apt-get). Instead, use the installation via zip.**

We are currently using phpLdapAdmin to graphically manage users on LDAP. This software is very old and is not being officially maintained. But works.

Download at: https://github.com/leenooks/phpLDAPadmin

Don't forget to

    cp cp config/config.php{.example,}

### Creating groups and users organizational units:

Obs: change `dc=domain,dc=org,dc=br` by your actual `dc`.

In phpLdapAdmin (or other ldap manager), we create two new organizational units. Click in "create->new generic organizational unit", and then create _users_. Afterwards, do the same and create _groups_. So, they will be called `dn=groups,dc=domain,dc=org,dc=br` and `dn=users,dc=domain,dc=org,dc=br`.

### Configuring LDAP User and group backend at Nextcloud

In NextCloud, the app _LDAP User and group backend_ must be enabled and then configured properly. Configuration is quite complicated, so a good way to configure it is to insert SQL directly in mysql, with following commands:

```
DELETE FROM `oc_appconfig` WHERE appid='user_ldap';

INSERT INTO `oc_appconfig` (`appid`, `configkey`, `configvalue`) VALUES
('user_ldap', 'cleanUpJobOffset', '0'),
('user_ldap', 'enabled', 'yes'),
('user_ldap', 'has_memberof_filter_support', '0'),
('user_ldap', 'home_folder_naming_rule', ''),
('user_ldap', 'installed_version', '1.2.1'),
('user_ldap', 'last_jpegPhoto_lookup', '0'),
('user_ldap', 'ldap_agent_password', 'MQ=='),
('user_ldap', 'ldap_attributes_for_group_search', ''),
('user_ldap', 'ldap_attributes_for_user_search', ''),
('user_ldap', 'ldap_backup_host', ''),
('user_ldap', 'ldap_backup_port', ''),
('user_ldap', 'ldap_base', 'dc=domain,dc=org,dc=br'),
('user_ldap', 'ldap_base_groups', 'ou=groups,dc=domain,dc=org,dc=br'),
('user_ldap', 'ldap_base_users', 'ou=users,dc=domain,dc=org,dc=br'),
('user_ldap', 'ldap_cache_ttl', '600'),
('user_ldap', 'ldap_configuration_active', '1'),
('user_ldap', 'ldap_default_ppolicy_dn', ''),
('user_ldap', 'ldap_display_name', 'displayName'),
('user_ldap', 'ldap_dn', 'cn=admin,dc=domain,dc=org,dc=br'),
('user_ldap', 'ldap_dynamic_group_member_url', ''),
('user_ldap', 'ldap_email_attr', 'mail'),
('user_ldap', 'ldap_experienced_admin', '0'),
('user_ldap', 'ldap_expert_username_attr', 'cn'),
('user_ldap', 'ldap_expert_uuid_group_attr', ''),
('user_ldap', 'ldap_expert_uuid_user_attr', ''),
('user_ldap', 'ldap_gid_number', 'gidNumber'),
('user_ldap', 'ldap_group_display_name', 'cn'),
('user_ldap', 'ldap_group_filter', '(objectclass=posixGroup)'),
('user_ldap', 'ldap_group_filter_mode', '1'),
('user_ldap', 'ldap_group_member_assoc_attribute', 'memberUid'),
('user_ldap', 'ldap_groupfilter_groups', ''),
('user_ldap', 'ldap_groupfilter_objectclass', ''),
('user_ldap', 'ldap_host', 'localhost'),
('user_ldap', 'ldap_login_filter', '(&(|(objectclass=inetOrgPerson)(objectclass=posixAccount)(objectclass=top))(|(cn=%uid)))'),
('user_ldap', 'ldap_login_filter_mode', '1'),
('user_ldap', 'ldap_loginfilter_attributes', ''),
('user_ldap', 'ldap_loginfilter_email', '0'),
('user_ldap', 'ldap_loginfilter_username', '1'),
('user_ldap', 'ldap_nested_groups', '0'),
('user_ldap', 'ldap_override_main_server', ''),
('user_ldap', 'ldap_paging_size', '500'),
('user_ldap', 'ldap_port', '389'),
('user_ldap', 'ldap_quota_attr', ''),
('user_ldap', 'ldap_quota_def', ''),
('user_ldap', 'ldap_tls', '0'),
('user_ldap', 'ldap_turn_off_cert_check', '0'),
('user_ldap', 'ldap_turn_on_displayname_change', '1'),
('user_ldap', 'ldap_turn_on_pwd_change', '0'),
('user_ldap', 'ldap_user_display_name_2', ''),
('user_ldap', 'ldap_user_filter_mode', '1'),
('user_ldap', 'ldap_userfilter_groups', ''),
('user_ldap', 'ldap_userfilter_objectclass', ''),
('user_ldap', 'ldap_userlist_filter', '(|(objectclass=inetOrgPerson)(objectclass=posixAccount)(objectclass=top))'),
('user_ldap', 'types', 'authentication'),
('user_ldap', 'use_memberof_to_detect_membership', '1');
```

Then, don't forget to go to administrator in Nextcloud, and set the correct password for the admin user. This password was created when you installed LDAP support in the server.

### Configuring Matrix to allow LDAP authentication

Please look here https://github.com/matrix-org/matrix-synapse-ldap3 to configure Matrix for LDAP authentication.

## Checkpoint

At this point, we are supposed to have:
* An LDAP server running, with some user and groups manually created (via PHPLdapAdmin);
* A Nextcloud instance running, with LDAP authentication enabled (and working - LDAP user should be able to login to Nextcloud)
* A Matrix server running, a Riot instance running - LDAP users should be able to login to Riot)

## Moving forward - connecting the pieces

### User Ldap Extended

Nextcloud core LDAP auth app allows the system to read LDAP user base, but not write. Thus, we developed a Nextcloud App to turn Nextcloud users functions (create/delete user/groups) into LDAP functions. So, instead of creating/deleting Nextcloud users/groups, all user operations on Nextcloud will now write to your LDAP database.

From Nextcloud root:

    cd apps
    git clone git@gitlab.com:eita/rios/user_ldap_extended.git

### Matrix Bridge

The Matrixbridge app was developed by EITA in order to connect Matrix and Nextcloud. Main features are:

* Files sent to a Riot room are stored in Nextcloud;
* When a room and a folder are connected, every file uploaded in this folder is shown at the room (and vice-versa)
* Calendars modifications are also shown in the connected rooms
* When a user is added to a NextCloud group, it is automatically added to a configured Matrix room

From Nextcloud root:

    cd apps
    git clone git@gitlab.com:eita/rios/matrixbridge.git
    cd matrixbridge
    git checkout refactoring
    git submodule update --init --recursive

### Npm and Yarn

Before installing calendar and tasks, npm and yarn must be installed. 

    sudo apt-get install npm
    sudo npm install -g yarn

### Circles

From Nextcloud root:

```
cd apps
git clone git@gitlab.com:eita/rios/nextcloud-circles.git circles --branch rios-vivos
chmod a-w circles -R
```

### Calendar

From Nextcloud root:

```
cd apps
git clone git@gitlab.com:eita/rios/cloud-calendar.git calendar --branch rios-vivos
cd circles
make
chmod a-w -R
```

### Configuring Applications Services in Synapse

After installing Synapse, we need to create the Application Service config file, homeserver-my-as.yaml, saved in matrix root:

```
id: my-as
url: https://[nextcloud url]/index.php/apps/matrixbridge/matrix_api
as_token: as_token
hs_token: hs_token
sender_localpart: logging
namespaces:
#  users:
#    - exclusive: false
#      regex: ".*"
  rooms:
    - exclusive: false
      regex: ".*"
  aliases:
    - exclusive: false
      regex: ".*"
```

Warning: We had to disable listening to users' events from the Application Service, since there is a bug with notifications badges, which will soon be fixed in a future release of Synapse. For the time being, the Rios' Application Service doesn't track users' events.

Then we must configure the homeserver.yaml, and also point to rios-as and to LDAP configuration at homeserver.yaml:

```
# A list of application service config file to use
app_service_config_files:  
- "path-to-synapse/homeserver-my-as.yaml"
```

### Running Synapse

First, get into the directory "path-to-synapse", then run the following commands to run the Synapse Server:

```
virtualenv -p python2.7 env 
source env/bin/activate
./synctl start
```

## Checkpoint 2

At this point, you should be able to create users and groups into LDAP from the Nextcloud user interface. Also, you should have the following NextCloud apps installed and enabled:
* Matrixbridge
* User Ldap Extended
* Circles
* Calendar
* Tasks


## Usage

In principle, users can comunicate to other users if they belong to the same Group. You can associate a Room to Group, so that everytime someone enters into a Group, it enters automatically into a Room:

### Set Default Room for a group

Setting default rooms (riot) for a group (nextcloud).

First, you need to have a user which must be admin of the desired room; we call it admin_chat.

1. Create the desired room with the user admin_chat
2. Insert this entry on the database: INSERT INTO oc_matrixbridge_group_rooms (gid, room_id) VALUES ('group name','room id');
3. On NextCloud, add (or remove) the user to the group

### Associate a room to a folder

In order to have a room connected to a folder, we'll use circles. If everything is working fine, every room in Riot is already a circle in NextCloud. Now, all you need is to share the desired folder with the desired circle (i.e., room). From now on, files uploaded to the circle will show on the room, and vice-versa. The same reasoning applies to Calendars.


### Ethercalc

In shell:

```
ethercalc --host rios.org.br --port 9000 --keyfile ./ssl/star.rios.org.br.key --certfile ./ssl/star.rios.org.br.crt > logs/etherpad.log 2> logs/etherpad_errors.log &
```

### Postgres

Synapse uses postgresql.

To restart postgres, use:

    sudo service postgresql {start|stop|status}